from collections import defaultdict


def build_graph(edges):
    # edges = [
    #     ["A", "B"], ["A", "E"],
    #     ["A", "C"], ["B", "D"],
    #     ["B", "E"], ["C", "F"],
    #     ["C", "G"], ["D", "E"]
    # ]
    graph = defaultdict(list)

    for edge in edges:
        a, b = edge[0], edge[1]

        graph[a].append(b)
        graph[b].append(a)
    return graph


def find_shortest_path(graph, start, goal):
    operations_count = 0
    explored = []

    queue = [[start]]

    if start == goal:
        operations_count += 1
        return None, operations_count

    while queue:
        path = queue.pop(0)
        node = path[-1]
        operations_count += 2

        if node not in explored:
            neighbours = graph[node]
            operations_count += 1

            for neighbour in neighbours:
                new_path = list(path)
                new_path.append(neighbour)
                queue.append(new_path)
                operations_count += 3

                if neighbour == goal:
                    operations_count += 1
                    print(f'Граф:\n{graph}\nКратчайший путь от {start} к {goal}:\n{new_path}')
                    return new_path, operations_count
            explored.append(node)
            operations_count += 1
    return None, operations_count
